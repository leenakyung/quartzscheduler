<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>스케줄러1</title>
	<script src="/quartz1/lib/jQuery/jquery-3.4.1.min.js"></script>
	<script src="/quartz1/lib/jQuery/jquery-3.4.1.js"></script>
	<script src="/quartz1/lib/BootStrap/js/bootstrap.min.js"></script>
	<script src="/quartz1/js/Scheduler.js"></script>
	<link rel="stylesheet" href="/quartz1/lib/BootStrap/css/bootstrap.css">
	<link rel="stylesheet" href="/quartz1/lib/BootStrap/css/bootstrap-theme.css">
</head>
<body>
	<h1 class = "text-center">
		스케줄러 상태 페이지
	</h1>
	<h2>● 스케줄러 상태 제어</h2>
	<div>
	<table>
	<tr>
		<td><select name = "accessTime1" id = "accessTime1" style="width:50.68px;height:29.26px;">
		<option value = "10"> 10초 </option>
		<option value = "20"> 20초 </option>
		<option value = "30"> 30초 </option>
		<option value = "40"> 40초 </option>
		<option value = "50"> 50초 </option>
		<option value = "60"> 60초 </option>
		</select></td>
		<td><button type="button" class ="btn btn-info btn-sm" id = "RunBtn1">Run1</button></td>
		<td><button type="button" class ="btn btn-info btn-sm" id = "StopBtn1">Stop1</button></td>
	</tr>
	<tr>
		<td><select name = "accessTime2" id = "accessTime2" style="width:50.68px;height:29.26px;">
		<option value = "10"> 10초 </option>
		<option value = "20"> 20초 </option>
		<option value = "30"> 30초 </option>
		<option value = "40"> 40초 </option>
		<option value = "50"> 50초 </option>
		<option value = "60"> 60초 </option>
		</select></td>
		<td><button type="button" class ="btn btn-info btn-sm" id = "RunBtn2">Run2</button></td>
		<td><button type="button" class ="btn btn-info btn-sm" id = "StopBtn2">Stop2</button></td>
	 </tr>
	 </table>
	 
	 </div>
		<h2>● 실행 목록 </h2>
		
		 <div id='quartzList'></div>
</body>
</html>