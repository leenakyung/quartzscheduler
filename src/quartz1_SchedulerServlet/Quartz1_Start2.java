package quartz1_SchedulerServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.google.gson.Gson;

import quartz1_Scheduler.Quartz_Def_Vo;
import quartz1_Scheduler.Update_Quartz1;


@WebServlet("/Quartz1_Start2")
public class Quartz1_Start2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private SchedulerFactory schedulerFactory;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		Connection conn = null;
		PreparedStatement Insert_DefPstmt = null;
		int second1 = Integer.parseInt(request.getParameter("chooseSecond2"));
		
		PreparedStatement pstmtStatus = null;
		ResultSet rsStatus = null;
		List<Quartz_Def_Vo> quartzStatus = new ArrayList<Quartz_Def_Vo>();
		
		
		try {

			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/Oracle11g");
			conn = ds.getConnection();
			
			schedulerFactory = new StdSchedulerFactory();
			Scheduler scheduler1 = schedulerFactory.getScheduler();
			scheduler1 = schedulerFactory.getScheduler();
			scheduler1.start();
			
			JobDetail job1 = JobBuilder.newJob(Update_Quartz1.class)
									   .withIdentity("job1" , "group1")
									   .build();

			SimpleTrigger simpleTigger1 = TriggerBuilder.newTrigger()
									   				   .withIdentity("trigger1", "group1")
									   				   .startNow()
									   				   .withSchedule(SimpleScheduleBuilder.simpleSchedule()	
									   				   .withIntervalInSeconds(second1)
									   				   .repeatForever())
									   				   .build();
			scheduler1.scheduleJob(job1,simpleTigger1);
			Insert_DefPstmt = conn.prepareStatement("MERGE INTO quartz_def USING dual ON(id = 'Sch#2') WHEN MATCHED THEN "
					+ "UPDATE SET start_time = to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),term = ?,status = 'Running'"
					+ "WHEN NOT MATCHED THEN  INSERT (id, start_time,term,status) VALUES ('Sch#2',to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),?,'Running')");
			Insert_DefPstmt.setInt(1, second1);
			Insert_DefPstmt.setInt(2, second1);
			Insert_DefPstmt.executeUpdate();
			
			pstmtStatus = conn.prepareStatement("select status from quartz_def where (id = 'Sch#2') and (rownum <=1)");
			rsStatus = pstmtStatus.executeQuery();
			
			while (rsStatus.next()) {
				Quartz_Def_Vo Quartz_Def_Status = new Quartz_Def_Vo();
				Quartz_Def_Status.setStatus(rsStatus.getString("status"));
				quartzStatus.add(Quartz_Def_Status);
			}
			Gson gson = new Gson();
			String jsonPlacetest = gson.toJson(quartzStatus);
			PrintWriter out = response.getWriter();
			out.write(jsonPlacetest);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}
			}
		}
	}

}

