package quartz1_SchedulerServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import com.google.gson.Gson;
import quartz1_Scheduler.Insert_Quartz1;
import quartz1_Scheduler.Quartz1_Vo;
import quartz1_Scheduler.Quartz_Def_Vo;
import quartz1_Scheduler.Update_Quartz1;
public class Quartz1_List extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
 
	@Override
	public void init(ServletConfig config) throws ServletException {
		Connection conn = null;
		SchedulerFactory schedulerFactory;
		PreparedStatement pstmtList = null;
		ResultSet rsList = null;
		int second = 0;

		try {
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/Oracle11g");
			conn = ds.getConnection();
		
			pstmtList = conn.prepareStatement("select term from quartz_def where (id = 'Sch#1')and (status = 'Running')");
			rsList = pstmtList.executeQuery();

			while (rsList.next()) {
				second = rsList.getInt("term");
			}
			
			if(second != 0) {
				schedulerFactory = new StdSchedulerFactory();
				Scheduler scheduler = schedulerFactory.getScheduler();
				scheduler = schedulerFactory.getScheduler();
				scheduler.start();
				
				JobDetail job = JobBuilder.newJob(Insert_Quartz1.class)
										   .withIdentity("job" , "group")
										   .build();
		
				SimpleTrigger simpleTigger = TriggerBuilder.newTrigger()
										   				   .withIdentity("trigger", "group")
										   				   .startNow()
										   				   .withSchedule(SimpleScheduleBuilder.simpleSchedule()	
										   				   .withIntervalInSeconds(second)
										   				   .repeatForever())
										   				   .build();
				scheduler.scheduleJob(job,simpleTigger);
			}
			pstmtList = conn.prepareStatement("select term from quartz_def where (id = 'Sch#2')and (status = 'Running')");
			rsList = pstmtList.executeQuery();
	
			while (rsList.next()) {
				second = rsList.getInt("term");
			}
			if(second != 0) {
				schedulerFactory = new StdSchedulerFactory();
				Scheduler scheduler1 = schedulerFactory.getScheduler();
				scheduler1 = schedulerFactory.getScheduler();
				scheduler1.start();
				
				JobDetail job1 = JobBuilder.newJob(Update_Quartz1.class)
										   .withIdentity("job1" , "group1")
										   .build();
		
				SimpleTrigger simpleTigger1 = TriggerBuilder.newTrigger()
										   				   .withIdentity("trigger1", "group1")
										   				   .startNow()
										   				   .withSchedule(SimpleScheduleBuilder.simpleSchedule()	
										   				   .withIntervalInSeconds(second)
										   				   .repeatForever())
										   				   .build();
				scheduler1.scheduleJob(job1,simpleTigger1);
			}
//		super.init(config);
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}
			}
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		Connection conn = null;
		
		try {

			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/Oracle11g");
			conn = ds.getConnection();
			
			List<Quartz1_Vo> list = getquartzList(conn);
			List<Quartz_Def_Vo> Sch1term =  getTermSch1(conn);
			List<Quartz_Def_Vo> Sch2term =  getTermSch2(conn);
			List<Quartz_Def_Vo> Sch1Status =  getQuartzSch1Status(conn);
			List<Quartz_Def_Vo> Sch2Status =  getQuartzSch2Status(conn);
			
			Gson gson = new Gson();
			Map<String,List> values = new HashMap<String,List>();
			values.put("list", list);
			values.put("termSch1", Sch1term);
			values.put("termSch2", Sch2term);
			values.put("statusSch1", Sch1Status);
			values.put("statusSch2", Sch2Status);
			
			String jsonPlacetest = gson.toJson(values);
			PrintWriter out = response.getWriter();
			out.write(jsonPlacetest);
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<Quartz1_Vo> getquartzList(Connection conn) throws Exception {

		List<Quartz1_Vo> quartzList = new ArrayList<Quartz1_Vo>();
		PreparedStatement pstmtList = null;
		ResultSet rsList = null;

		try {
			// 리스트 출력
			pstmtList = conn.prepareStatement("select * from quartz1 ORDER BY num desc");
			rsList = pstmtList.executeQuery();

			while (rsList.next()) {
				Quartz1_Vo quartzInfo = new Quartz1_Vo();
				quartzInfo.setNum(rsList.getInt("num"));
				quartzInfo.setType1(rsList.getString("type1"));
				quartzInfo.setType1_Time(rsList.getString("type1_Time"));
				quartzInfo.setType2(rsList.getString("type2"));
				quartzInfo.setType2_Time(rsList.getString("type2_Time"));
				quartzList.add(quartzInfo);
			}

		} catch (Exception e) {
			throw e;
		} finally {
			if (rsList != null) {
				try {
					rsList.close();
				} catch (Exception e) {
				}
			}
			if (pstmtList != null) {
				try {
					pstmtList.close();
				} catch (Exception e) {
				}
			}
		}
		return quartzList;	
	}
	public List<Quartz_Def_Vo> getTermSch1(Connection conn) throws Exception{
		
		List<Quartz_Def_Vo> quartztermSch1 = new ArrayList<Quartz_Def_Vo>();
		PreparedStatement pstmQuartzTermSch1 = null;
		ResultSet rsQuartzTermSch1 = null;

		try {
			// 리스트 출력
			pstmQuartzTermSch1 = conn.prepareStatement("select term from quartz_def where (rownum <= 1)and(id = 'Sch#1')");
			rsQuartzTermSch1 = pstmQuartzTermSch1.executeQuery();

			while (rsQuartzTermSch1.next()) {
				Quartz_Def_Vo TermInfo = new Quartz_Def_Vo();
				TermInfo.setTerm(rsQuartzTermSch1.getInt("term"));
				quartztermSch1.add(TermInfo);
			}

		} catch (Exception e) {
			throw e;
		} finally {
			if (rsQuartzTermSch1 != null) {
				try {
					rsQuartzTermSch1.close();
				} catch (Exception e) {
				}
			}
			if (pstmQuartzTermSch1 != null) {
				try {
					pstmQuartzTermSch1.close();
				} catch (Exception e) {
				}
			}
		}
		return quartztermSch1;	
	}
	public List<Quartz_Def_Vo> getTermSch2(Connection conn) throws Exception{
		
		List<Quartz_Def_Vo> quartzTermSch2 = new ArrayList<Quartz_Def_Vo>();
		PreparedStatement pstmQuartzTermSch2 = null;
		ResultSet rsQuartzTermSch2 = null;

		try {
			// 리스트 출력
			pstmQuartzTermSch2 = conn.prepareStatement("select term from quartz_def where (rownum <= 1) and (id = 'Sch#2') ");
			rsQuartzTermSch2 = pstmQuartzTermSch2.executeQuery();

			while (rsQuartzTermSch2.next()) {
				Quartz_Def_Vo TermInfo = new Quartz_Def_Vo();
				TermInfo.setTerm(rsQuartzTermSch2.getInt("term"));
				quartzTermSch2.add(TermInfo);
			}

		} catch (Exception e) {
			throw e;
		} finally {
			if (rsQuartzTermSch2 != null) {
				try {
					rsQuartzTermSch2.close();
				} catch (Exception e) {
				}
			}
			if (pstmQuartzTermSch2 != null) {
				try {
					pstmQuartzTermSch2.close();
				} catch (Exception e) {
				}
			}
		}
		return quartzTermSch2;	
	}
	public List<Quartz_Def_Vo> getQuartzSch1Status(Connection conn) throws Exception{
		
		List<Quartz_Def_Vo> quartz1Status = new ArrayList<Quartz_Def_Vo>();
		PreparedStatement pstmQuartz1Status = null;
		ResultSet rsQuartz1Status = null;

		try {
			pstmQuartz1Status = conn.prepareStatement("select status from quartz_def where (rownum <= 1)and(id = 'Sch#1')");
			rsQuartz1Status = pstmQuartz1Status.executeQuery();

			while (rsQuartz1Status.next()) {
				Quartz_Def_Vo statusInfo = new Quartz_Def_Vo();
				statusInfo.setStatus(rsQuartz1Status.getString("status"));
				quartz1Status.add(statusInfo);
			}

		} catch (Exception e) {
			throw e;
		} finally {
			if (rsQuartz1Status != null) {
				try {
					rsQuartz1Status.close();
				} catch (Exception e) {
				}
			}
			if (pstmQuartz1Status != null) {
				try {
					pstmQuartz1Status.close();
				} catch (Exception e) {
				}
			}
		}
		return quartz1Status;
	}
	public List<Quartz_Def_Vo> getQuartzSch2Status(Connection conn) throws Exception{
		
		List<Quartz_Def_Vo> quartz2Status = new ArrayList<Quartz_Def_Vo>();
		PreparedStatement pstmQuartz2Status = null;
		ResultSet rsQuartz2Status = null;

		try {
			pstmQuartz2Status = conn.prepareStatement("select status from quartz_def where (rownum <= 1)and(id = 'Sch#2')");
			rsQuartz2Status = pstmQuartz2Status.executeQuery();

			while (rsQuartz2Status.next()) {
				Quartz_Def_Vo statusInfo = new Quartz_Def_Vo();
				statusInfo.setStatus(rsQuartz2Status.getString("status"));
				quartz2Status.add(statusInfo);
			}

		} catch (Exception e) {
			throw e;
		} finally {
			if (rsQuartz2Status != null) {
				try {
					rsQuartz2Status.close();
				} catch (Exception e) {
				}
			}
			if (pstmQuartz2Status != null) {
				try {
					pstmQuartz2Status.close();
				} catch (Exception e) {
				}
			}
		}
		return quartz2Status;	
	}
	

}