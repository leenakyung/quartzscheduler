package quartz1_SchedulerServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

import com.google.gson.Gson;

import quartz1_Scheduler.Quartz_Def_Vo;

@WebServlet("/Quartz1_Stop1")
public class Quartz1_Stop1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private SchedulerFactory schedulerFactory = new StdSchedulerFactory();
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		Connection conn = null;
		PreparedStatement Insert_DefPstmt = null;
		
		PreparedStatement pstmtStatus = null;
		ResultSet rsStatus = null;
		List<Quartz_Def_Vo> quartzStatus = new ArrayList<Quartz_Def_Vo>();
		
		JobKey jobKey = new JobKey("job", "group");
		try {

			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/Oracle11g");
			conn = ds.getConnection();

			Scheduler scheduler = schedulerFactory.getScheduler();
			scheduler.deleteJob(jobKey);
			
			Insert_DefPstmt = conn.prepareStatement("update quartz_def set end_Time = to_char(sysdate,'YYYY-MM-DD HH24:MI:SS'),status = 'Stopped' where id = 'Sch#1'");
			Insert_DefPstmt.executeUpdate();
			
			pstmtStatus = conn.prepareStatement("select status from quartz_def where (id = 'Sch#1') and (rownum <=1)");
			rsStatus = pstmtStatus.executeQuery();
			
			while (rsStatus.next()) {
				Quartz_Def_Vo Quartz_Def_Status = new Quartz_Def_Vo();
				Quartz_Def_Status.setStatus(rsStatus.getString("status"));
				quartzStatus.add(Quartz_Def_Status);
			}
			Gson gson = new Gson();
			String jsonPlacetest = gson.toJson(quartzStatus);
			PrintWriter out = response.getWriter();
			out.write(jsonPlacetest);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}
			}
		}
	}
}
