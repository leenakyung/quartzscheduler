$(document).ready(function() {
	List();
	
	$("#RunBtn1").click(function() {
		Run1();
	});

	$("#StopBtn1").click(function() {
		Stop1();
	});
	
	$("#RunBtn2").click(function() {
		Run2();
	});

	$("#StopBtn2").click(function() {
		Stop2();
	});
	
});

	function List() {
		$.ajax({
				type : "GET",
				url : "/quartz1/Quartz1_List",
				datatype : 'json',
				success : function(data) {
					var obj = JSON.parse(data);
					var str = "<table class=\"table table-hover table table-bordered\">";
					str += "<tr>";
					str += "<td class=\"text-center success col-md-1\">INDEX</td>";
					str += "<td class=\"text-center success col-md-1\">TYPE1</td>";
					str += "<td class=\"text-center success col-md-1\">TYPE1_TIME</td>";
					str += "<td class=\"text-center success col-md-1\">TYPE2</td>";
					str += "<td class=\"text-center success col-md-1\">TYPE2_TIME</td>";
					str += "</tr>";
					//리스트 출력
					for (var i = 0; i < obj.list.length; i++) {
						var num = obj.list[i].Num;
						var type1 = obj.list[i].Type1;
						var type1_Time = obj.list[i].Type1_Time;
						var type2 = obj.list[i].Type2;
						var type2_Time = obj.list[i].Type2_Time;

						str += "<tr><td> " + obj.list[i].Num + " </td>";
						str += "<td>" + obj.list[i].Type1 + " </td>";
						str += "<td>" + obj.list[i].Type1_Time + "</td>";
						str += "<td>" + obj.list[i].Type2 + "</td>";
						str += "<td>" + obj.list[i].Type2_Time + "</td></tr>";
					}
					str += "</table>";
					$('#quartzList').append(str);
					
					//Sch#1 term 유지
					var secondSch1 = obj.termSch1[0].Term;
					var artSch1 = secondSch1;
					$("#accessTime1").val(artSch1);
					//Sch#2 term 유지
					var secondSch2 = obj.termSch2[0].Term;
					var atrSch2 = secondSch2;
					$("#accessTime2").val(atrSch2);
					
					//Sch#1 status 유지
					var statusSch1 = obj.statusSch1[0].Status;
					var dtrSch1 = statusSch1;
					if(dtrSch1 == "Running"){
						$("#RunBtn1").html(dtrSch1);
						$("#StopBtn1").text("Stop1");
						$("#RunBtn1").attr("disabled",true);
					}else if(dtrSch1 =="Stopped"){
						$("#StopBtn1").html(dtrSch1);
						$("#RunBtn1").text("Run1");
						$("#StopBtn1").attr("disabled",true);
					}
				    //Sch#1 status 유지
					var statusSch2 = obj.statusSch2[0].Status;
					var dtrSch2 = statusSch2;
					if(dtrSch2 == "Running"){
						$("#RunBtn2").html(dtrSch2);
						$("#StopBtn2").text("Stop1");
						$("#RunBtn2").attr("disabled",true);
					}else if(dtrSch2 =="Stopped"){
						$("#StopBtn2").html(dtrSch2);
						$("#RunBtn2").text("Run1");
						$("#StopBtn2").attr("disabled",true);
					}
				},
				error : function(data) {
					alert("실패");
				}
			});
	}
	
	function Run1(){
		var chooseSecond1 = $("#accessTime1 option:selected").val();
		$.ajax({
			type : "GET",
			url : "/quartz1/Quartz1_Start1",
			datatype : 'json',
			data : {
				chooseSecond1 : chooseSecond1
			},
			success : function(data) {
					var obj = JSON.parse(data);
					var str = '';
					for(var i =0; i<obj.length; i++){
						var status = obj[0].Status; 
						str += '<span>'+obj[0].Status+'</span>';
					}
					$("#RunBtn1").html(str);
					$("#StopBtn1").text("Stop1");
					$("#RunBtn1").attr("disabled",true);
					$("#StopBtn1").removeAttr("disabled");
			},
			error : function(data) {
				alert("실패");
			}
		});
	}
	
	function Stop1(){
		$.ajax({
			type : "GET",
			url : "/quartz1/Quartz1_Stop1",
			datatype : 'json',
			success : function(data) {
				var obj = JSON.parse(data);
				var str = '';
				for(var i =0; i<obj.length; i++){
					var status = obj[i].Status; 
					
					str += '<span>'+obj[i].Status+'</span>';
				}
				$("#StopBtn1").html(str);
				$("#RunBtn1").text("Run1");
				$("#StopBtn1").attr("disabled",true);
				$("#RunBtn1").removeAttr("disabled");
			},
			error : function(data) {
				alert("실패");
			}
		});
	}
	
	function Run2(){
		var chooseSecond2 = $("#accessTime2 option:selected").val();
		$.ajax({
			type : "GET",
			url : "/quartz1/Quartz1_Start2",
			datatype : 'json',
			data : {
				chooseSecond2 : chooseSecond2
			},
			success : function(data) {
				var obj = JSON.parse(data);
				var str = '';
				for(var i =0; i<obj.length; i++){
					var status = obj[i].Status; 
					
					str += '<span>'+obj[i].Status+'</span>';
				}
				$("#RunBtn2").html(str);
				$("#StopBtn2").text("Stop2");
				$("#RunBtn2").attr("disabled",true);
				$("#StopBtn2").removeAttr("disabled");
			},
			error : function(data) {
				alert("실패");
			}
		});
	}
	
	function Stop2(){
		$.ajax({
			type : "GET",
			url : "/quartz1/Quartz1_Stop2",
			datatype : 'json',
			success : function(data) {
				var obj = JSON.parse(data);
				var str = '';
				for(var i =0; i<obj.length; i++){
					var status = obj[i].Status; 
					
					str += '<span>'+obj[i].Status+'</span>';
				}
				$("#StopBtn2").html(str);
				$("#RunBtn2").text("Run2");
				$("#StopBtn2").attr("disabled",true);
				$("#RunBtn2").removeAttr("disabled");
			},
			error : function(data) {
				alert("실패");
			}
		});
	}